<?php

use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

require_once __DIR__  . '/../database/Conexao.php';

final class ViewController
{

    public function index(Request $request,Response $response, $args)
    {
        $view = Twig::fromRequest($request);
        return $view->render($response, 'index.twig', ["title" => "Home"]);
    }

    public function clientes(Request $request,Response $response, $args)
    {
        try{
         $db = new Conexao();
         $cnx = $db->conectar();
         $pstm = $cnx->prepare("SELECT * FROM clientes");
         $pstm->execute();
         $result = $pstm->fetchAll(PDO::FETCH_ASSOC);
         $view = Twig::fromRequest($request);
         return $view->render($response, 'clientes.twig', ["title" => "Clientes","clientes"=>$result]);
        }catch(PDOException $ex){
            return $response->getBody()->write(json_encode($ex->getMessage(), JSON_UNESCAPED_UNICODE, JSON_UNESCAPED_SLASHES));
            
        }
    }
}
